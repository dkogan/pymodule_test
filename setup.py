#!/usr/bin/python2

from distutils.core import setup
from distutils.extension import Extension

setup(name='tst',
      version='1.0',
      ext_package='.',
      ext_modules=[Extension('tstmod', ['pymod.c'])])
