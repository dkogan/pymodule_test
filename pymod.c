#include <Python.h>
#include <stdio.h>

static PyObject* f_py(PyObject* self __attribute__((unused)),
                      PyObject* args __attribute__((unused)))
{
    printf("in f()\n");
    Py_RETURN_NONE;
}


PyMODINIT_FUNC inittstmod(void)
{
    static PyMethodDef methods[] =
        { {"f", (PyCFunction)f_py, METH_NOARGS, "tstmod.f()\n"},
          {}
        };


    PyImport_AddModule("tstmod");
    Py_InitModule3("tstmod", methods,
                   "Python test module");
}
